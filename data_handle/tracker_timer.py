from daily_report import daily_report
from twisted.internet import task  # 定时任务
from twisted.internet import reactor
import random,datetime,time
import redis
#连接REDIS
pool_redis= redis.ConnectionPool(host='localhost',port=6379,decode_responses=True)
mesg_redis=redis.Redis(connection_pool=pool_redis)
# midnight = now +86400- (now % 86400) + time.timezone+3600*8.5
# timeArray = time.localtime(midnight)
# otherStyleTime = time.strftime("%Y--%m--%d %H:%M:%S", timeArray)
# print(otherStyleTime)
# print((now % 86400) - time.timezone,time.timezone)
# pre_midnight = midnight + 86400 

# 计算下首次启动的时间差，原则上，每天早上的8:30计算一次统计数据
now = time.time()
DELAYTIME = 86400- (now % 86400) + time.timezone+3600*8.5  # 定时任务轮询周期(秒／单位),即，第二天的早上8:30到现在的时间差


def func():
	# 首次启动之后，每天同一时间再次启动
	task1 = task.LoopingCall(daily_report)
	task1.start(86400)

reactor.callLater(DELAYTIME,func)

# 文件存储
def save_multidata_file():
	multidata=mesg_redis.rpop('save_multidata_file')
	if multidata!=None:
		print('ok')

# 定时清理，超过一定时间的数据，比如超过1个月的图片，音频，超过1年的历史数据等
# 定时清理temp文件夹，图片音频等临时数据


reactor.run()
